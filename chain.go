package chi

import (
	"net/http"

	"github.com/codegangsta/inject"
)

// Chain returns a Middlewares type from a slice of middleware handlers.
func Chain(middlewares ...middlewareType) Middlewares {
	return middlewares
}

// Handler builds and returns a http.Handler from the chain of middlewares,
// with `h http.Handler` as the final handler.
func (mws Middlewares) Handler(h interface{}) http.Handler {
	return &ChainHandler{mws, h, chain(mws, h)}
}

// HandlerFunc builds and returns a http.Handler from the chain of middlewares,
// with `h http.Handler` as the final handler.
func (mws Middlewares) HandlerFunc(h http.HandlerFunc) http.Handler {
	return &ChainHandler{mws, h, chain(mws, h)}
}

// ChainHandler is a http.Handler with support for handler composition and
// execution.
type ChainHandler struct {
	Middlewares Middlewares
	Endpoint    interface{}
	chain       http.Handler
}

func (c *ChainHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c.chain.ServeHTTP(w, r)
}

// chain builds a http.Handler composed of an inline middleware stack and endpoint
// handler in the order they are passed.
func chain(middlewares []middlewareType, endpoint interface{}) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		ctx := RouteContext(request.Context())
		h := wrapHandler(ctx.Injector, endpoint)
		if len(middlewares) > 0 {
			middlewares_ := wrapMiddleware(ctx.Injector, middlewares)
			for i := len(middlewares_) - 1; i >= 0; i-- {
				h = middlewares_[i](h)
			}
		}
		h.ServeHTTP(writer, request)
	})
}

func wrapHandler(ij inject.Injector, handler interface{}) http.Handler {
	var h http.Handler
	switch _h := handler.(type) {
	case http.Handler:
		h = _h
	case func(http.ResponseWriter, *http.Request):
		h = http.HandlerFunc(_h)
	case http.HandlerFunc:
		h = _h
	default:
		h = http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
			_, err := ij.Invoke(_h)
			if err != nil {
				panic(err)
			}
		})
	}
	return h
}

func wrapMiddleware(ij inject.Injector, middlewares []interface{}) []func(handler http.Handler) http.Handler {
	var stdMiddleware = make([]func(handler http.Handler) http.Handler, 0, len(middlewares))
	for _, middleware := range middlewares {
		switch v := middleware.(type) {
		case func(http.Handler) http.Handler:
			stdMiddleware = append(stdMiddleware, v)
		default:
			stdMiddleware = append(stdMiddleware, func(next http.Handler) http.Handler {
				return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					if ret, err := ij.Invoke(v); err != nil {
						panic(err)
					} else if len(ret) == 1 {
						if b, ok := ret[0].Interface().(bool); ok && b == false {
							return
						}
					}
					next.ServeHTTP(w, r)
				})
			})
		}
	}
	return stdMiddleware
}
